import time
import random

LIST_VALUE_MAX = 100 

def createRndList(ln):
    l = [random.randint(0, LIST_VALUE_MAX) for i in range(ln)]
    return l

def isSorted(l):
    for idx in range(len(l)-1):
        if l[idx] > l[idx+1]:
            return False
    return True

# Sorts the list by randomly rearranging it until it is sorted
def rndSort(l):
    shuffles_num_of = 0
    while not isSorted(l):
        random.shuffle(l) # Randomly rearrange
        shuffles_num_of += 1
    print("List sorted after %s shuffles" % shuffles_num_of)
    return l

def bubbleSort(l):
    swapped = 1
    while swapped != 0:
        swapped = 0
        for i in range(len(l)-1):
            if l[i] > l[i+1]:
                temp = l[i]
                l[i] = l[i+1]
                l[i+1] = temp
                swapped = swapped + 1
    return l

def rndSearch(l, e, steps_max=1000):
    step_count = 1
    for _ in range(steps_max):
        rnd_idx = random.randint(0,len(l)-1)
        if l[rnd_idx] == e:
            return step_count
        step_count += 1
    return -1

def linearSearch(l, e):
    for i in range(len(l)):
        if l[i] == e:
            return i 
    return -1

def binarySearch(l, e):
    stepcount = 1
    dist = len(l)//2
    i = dist
    while dist > 0:
        if l[i] == e:
            return stepcount
        else:
            dist = dist//2
            if e > l[i]:
                i = i + dist
            else:
                i = i - dist 
        stepcount = stepcount + 1
    return -1

l = createRndList(64)
print("List %s" % l)
print("Length %d" % len(l))
print("Sorted %s" % isSorted(l))

# start_time = time.time()
# l1 = rndSort(l)
# end_time = time.time()
# print("Random sorting took %f seconds" % (end_time - start_time))

start_time = time.time()
l2 = bubbleSort(l)
end_time = time.time()
print("Bubble sorting took %f seconds" % (end_time - start_time))

print("Sorted %s" % isSorted(l2))
print("Sorted list %s" % l2)

x = random.randint(0, LIST_VALUE_MAX)
print("Searching for %d" % x)
start_time = time.time()
res = rndSearch(l, x)
end_time = time.time()
print("Random search found value after %d steps" % res)
print("Random searching took %f seconds" % (end_time - start_time))

start_time = time.time()
res = linearSearch(l, x)
end_time = time.time()
print("Linear search found value after %d steps" % res)
print("Linear searching took %f seconds" % (end_time - start_time))

start_time = time.time()
res = binarySearch(l, x)
end_time = time.time()
print("Binary search found value after %d steps" % res)
print("Binary searching took %f seconds" % (end_time - start_time))